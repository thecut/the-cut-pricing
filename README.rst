The Cut Django App: Pricing


================
thecut.pricing
================

To install this application (whilst in the project's activated virtualenv)::
    pip install git+ssh://git@git.thecut.net.au/thecut-pricing

There is no need to add ``thecut.pricing`` to your project's
``INSTALLED_APPS`` setting, as it provides only abstract models.
