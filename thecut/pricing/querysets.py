# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.db import models
from .utils import filter_start_end_period


class AbstractPriceQuerySet(models.query.QuerySet):

    def active(self, at=None, during=None):
        """
        Finds prices which active at point in time 'at' or during the period
        that came in tuple 'during'.
        If both parameters are None it returns prices that active now.

        :param at: Timezone aware datetime, at which prices should be active.
        :param during: '(start datetime, end datetime)' period during which
            prices should be active.
        :return: Filtered '~AbstractPriceQuerySet'
        """
        return filter_start_end_period(self, at, during)