# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.utils import timezone
from django.db import models


def filter_start_end_period(queryset, at=None, during=None):
    """
    Filters queryset for models with 'start_at' and 'end_at' fields.
    Finds objects where 'at' falls between 'start_at' and 'end_at' or
    which overlap with period given in 'during'.

    Parameter 'at' is taken into account only if 'during' is None.

    :param at: A timezone aware datetime.
    :param during: A tuple of timezone aware datetimes which define the
        start and end times of the duration. If one is omitted it counts as
        "open ended" period. If `during` is present, but both ends are absent,
        this method just returns unfiltered queryset.

    :type at: :py:class:`~datetime.datetime`
    :type during: :py:class:`~Tuple`
    :returns: Filtered queryset.

    """
    if during is None:
        at = at or timezone.now()
        return queryset.filter(start_at__lte=at).filter(
            models.Q(end_at__gt=at) | models.Q(end_at__isnull=True))
    else:
        start, end = during
        if start and end:
            return queryset.filter(
                # Starts in our range
                models.Q(start_at__gte=start, start_at__lt=end) |
                # Ends in our range
                models.Q(end_at__lt=end, end_at__gt=start) |
                # Our range is within the instance range
                models.Q(start_at__lte=start, end_at__gt=end)|
                # Starts before the end of our range, and has no end
                models.Q(start_at__lt=end, end_at__isnull=True)
            )
        elif start:
            return queryset.filter(
                # Ends in our range
                models.Q(end_at__gt=start) |
                # Or has no end
                models.Q(end_at__isnull=True)
            )
        elif end:
            return queryset.filter(
                # Starts in our range
                models.Q(start_at__lt=end)
            )
        else:
            # Be nice. Someone sends during=(None, None).
            return queryset
