# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from thecut.publishing.models import PublishableResource
from thecut.authorship.models import Authorship
from django.db import models
from django.contrib.contenttypes import generic
from thecut.pricing import querysets
from model_utils.managers import PassThroughManager


class AbstractPricingPlan(PublishableResource):
    """Abstract base class for pricing plans which group prices together.
    """
    name = models.CharField(max_length=250)
    """Friendly name for the pricing plan."""

    class Meta(PublishableResource.Meta):
        abstract = True


class AbstractPrice(Authorship):
    """Abstract base class for prices which have start and end date.
    End date is optional. It has a generic foreign key to get object
    associated with price.
    """
    content_type = models.ForeignKey('contenttypes.ContentType',
                                     editable=False, related_name='+',
                                     null=True, blank=True)
    object_id = models.IntegerField(db_index=True, null=True, blank=True,
                                    editable=False)
    content_object = generic.GenericForeignKey('content_type', 'object_id')
    """Generic foreign key to object with this price.
    """

    start_at = models.DateTimeField(db_index=True)
    """The :py:class:`~datetime.date` this price comes into effect
    (required)."""

    end_at = models.DateTimeField(db_index=True, blank=True, null=True)
    """The :py:class:`~datetime.date` from which this price is no longer
    in effect."""

    net_price = models.DecimalField(max_digits=7, decimal_places=2)
    """The net price of the object when this price is in effect (required)."""

    gross_price = models.DecimalField(max_digits=7, decimal_places=2)
    """The gross price of the object when this price is in effect
    (required)."""

    tax_value = models.DecimalField(blank=True, null=True, max_digits=7,
                                    decimal_places=2)
    """Amount of tax in the gross price."""

    is_enabled = models.BooleanField('enabled', db_index=True, default=True)
    """Indicator that this price is actually active."""

    objects = PassThroughManager().for_queryset_class(
        querysets.AbstractPriceQuerySet)()

    class Meta(Authorship.Meta):
        abstract = True

    def is_active(self):
        queryset = self.__class__.objects.active()

        if self.object_id:
            queryset = queryset.filter(object_id=self.object_id)

        return queryset.filter(pk=self.pk).exists()